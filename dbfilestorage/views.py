from django.http import HttpResponse
from django.shortcuts import get_object_or_404

from .models import DBFile


def show_file(request, name):
    dbf = get_object_or_404(DBFile, name=name)
    return HttpResponse(
        dbf.b64.decode('base64'),
        content_type=dbf.content_type)
