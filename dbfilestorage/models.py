from django.db import models


# Create your models here.
class DBFile(models.Model):
    name = models.CharField(max_length=100)
    content_type = models.CharField(max_length=100)
    b64 = models.TextField()

    def __unicode__(self):
        return unicode(self.name)
